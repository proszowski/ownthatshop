using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ownthatshop.Model.Client;

namespace ownthatshop.Database.PostgreSql
{
    public class ClientTypeDatabase : IClientTypeRepository
    {
        private readonly OwnThatShopContext _context;
        private readonly DbSet<ClientType> _clientTypes;

        public ClientTypeDatabase(OwnThatShopContext context)
        {
            _context = context;
            _clientTypes = _context.ClientTypes;
        }

        public ActionResult<List<ClientType>> GetAllTypes()
        {
            return _clientTypes.ToList();
        }

        public void Add(ClientType clientClientType)
        {
            _clientTypes.Add(clientClientType);
            _context.SaveChanges();
        }

        public ClientType FindById(int? clientTypeId)
        {
            return _clientTypes.Find(clientTypeId);
        }
    }
}