using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using ownthatshop.Model.Client;

namespace ownthatshop.Database.PostgreSql
{
    public class ClientDatabase : IClientRepository
    {
        private readonly DbSet<Address> _addresses;
        private readonly DbSet<Client> _clients;
        private readonly DbSet<ClientType> _clientTypes;
        private readonly DbSet<Company> _companies;
        private readonly DbSet<Contact> _contacts;
        private readonly OwnThatShopContext _context;

        public ClientDatabase(OwnThatShopContext context)
        {
            _context = context;
            _clients = _context.Clients;
            _clientTypes = _context.ClientTypes;
            _addresses = context.Addresses;
            _contacts = context.Contacts;
            _companies = context.Companies;
        }

        public ActionResult<List<Client>> GetAll()
        {
            var clients = from client in _clients
                join address in _addresses on client.Address.Id equals address.Id
                join clientType in _clientTypes on client.ClientType.Id equals clientType.Id
                join contact in _contacts on client.Contact.Id equals contact.Id
                join company in _companies on client.Company.Id equals company.Id
                select new Client(client.Id, client.FirstName, client.Surname, clientType, contact, address, company);

            return clients.ToList();
        }

        public void Add(Client client)
        {
            var clientType = _clientTypes.Find(client.ClientType.Id);
            if (clientType != null) client.ClientType = clientType;
            _addresses.Add(client.Address);
            _contacts.Add(client.Contact);
            _companies.Add(client.Company);
            _clients.Add(client);
            _context.SaveChanges();
        }

        public void Update(Client client)
        {
            _addresses.Update(client.Address);
            _contacts.Update(client.Contact);
            client.ClientType = _clientTypes.Find(client.ClientType.Id);
            _clients.Update(client);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var client = GetAll().Value.Where(c => c.Id == id).FirstOr(null);
            if (client == null) return;
            _clients.Remove(client);
            _contacts.Remove(client.Contact);
            _addresses.Remove(client.Address);
            _context.SaveChanges();
        }

        public Client GetById(long? clientId)
        {
            return GetAll().Value.Find(c => c.Id == clientId);
        }
    }
}