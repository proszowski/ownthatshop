using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ownthatshop.Model.Product;
using ownthatshop.Model.Product.Dto;

namespace ownthatshop.Database.PostgreSql
{
    public class ProductDatabase : IProductRepository
    {
        private OwnThatShopContext _context;
        private DbSet<Product> _products;
        private DbSet<ProductType> _productTypes;

        public ProductDatabase(OwnThatShopContext context)
        {
            _context = context;
            _products = _context.Products;
            _productTypes = _context.ProductTypes;
        }

        public ActionResult<List<Product>> FindAll()
        {
            var resultSet = from product in _products
                join productType in _productTypes on product.ProductType.Id equals productType.Id
                select new
                {
                    Id = product.Id,
                    ProductName = product.Name,
                    Image = product.Image,
                    Quantity = product.Quantity,
                    Price = product.Price,
                    ProductType = new ProductType(productType.Id, productType.Name)
                };

            var productDtos = new List<Product>();
            foreach (var product in resultSet)
            {
                var productDto = ProductBuilder.aProductDto().withId(product.Id.Value).withName(product.ProductName)
                    .withImage(product.Image).withPrice(product.Price.Value).withQuantity(product.Quantity.Value)
                    .withType(product.ProductType).build();
                productDtos.Add(productDto);
            }

            return productDtos;
        }

        public ActionResult<Product> FindById(long id)
        {
            var products = FindAll();
            var wantedProduct = products.Value.FirstOrDefault(product => product.Id == id);
            return wantedProduct;
        }

        public void SaveProduct(Product product)
        {
            _products.Add(product);
            _context.SaveChanges();
        }

        public void RemoveProduct(long id)
        {
            var product = _products.Find(id);
            _products.Remove(product);
            _context.SaveChanges();
        }

        public void UpdateProduct(Product product)
        {
            _products.Update(product);
            _context.SaveChanges();
        }

        public ActionResult<ICollection<ProductType>> GetAvailableTypes()
        {
            return _productTypes.ToList();
        }
    }
}