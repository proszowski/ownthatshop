using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ownthatshop.Model.Client;
using ownthatshop.Model.Transaction;

namespace ownthatshop.Database.PostgreSql
{
    public class TransactionDatabase : ITransactionRepository
    {
        private OwnThatShopContext _context;
        private DbSet<Transaction> _transactions;
        private DbSet<TransactionDetails> _transactionDetails;

        public TransactionDatabase(OwnThatShopContext context)
        {
            _context = context;
            _transactions = context.Transactions;
            _transactionDetails = context.TransactionDetails;
        }
        public ActionResult<List<Transaction>> FindAll()
        {
            var transactions = _transactions.Include(t => t.TransactionDetails).ToList();
            return transactions;
        }

        public void Add(Transaction transaction)
        {
            _transactions.Add(transaction);
            _context.SaveChanges();
        }
    }
}