using Microsoft.EntityFrameworkCore;
using ownthatshop.Model.Client;
using ownthatshop.Model.Product;
using ownthatshop.Model.Transaction;

namespace ownthatshop.Database.PostgreSql
{
    public class OwnThatShopContext : DbContext
    {
        public OwnThatShopContext(DbContextOptions<OwnThatShopContext> options) : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }

        public DbSet<ProductType> ProductTypes { get; set; }
        public DbSet<Client> Clients { get; set; }

        public DbSet<ClientType> ClientTypes { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<TransactionDetails> TransactionDetails { get; set; }
    }
}