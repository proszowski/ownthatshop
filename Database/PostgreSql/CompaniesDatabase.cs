using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ownthatshop.Model.Client;

namespace ownthatshop.Database.PostgreSql
{
    public class CompaniesDatabase : ICompaniesRepository
    {
        private readonly OwnThatShopContext _context;
        private readonly DbSet<Company> _companies;

        public CompaniesDatabase(OwnThatShopContext context)
        {
            _context = context;
            _companies = _context.Companies;
        }

        public ActionResult<List<Company>> GetAllCompanies()
        {
            return _companies.ToList();
        }

        public void Add(Company company)
        {
            _companies.Add(company);
            _context.SaveChanges();
        }
    }
}