using System;

namespace ownthatshop.Service
{
    public class AvailabilityStatus
    {

        public Boolean IsAvailable { get; }
        public int ActualQuantity { get; }

        private AvailabilityStatus(Boolean isAvailable, int quantity)
        {
            IsAvailable = isAvailable;
            ActualQuantity = quantity;
        }

        public static AvailabilityStatus available(int quantity)
        {
            return new AvailabilityStatus(true, quantity);
        }

        public static AvailabilityStatus notAvailable(int quantity)
        {
            return new AvailabilityStatus(false, quantity);
        }
    }
}