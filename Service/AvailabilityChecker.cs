using System;
using ownthatshop.Model.Product;
using ownthatshop.Model.Product.Exception;
using ownthatshop.Model.Transaction;

namespace ownthatshop.Service
{
    public class AvailabilityChecker
    {
        public AvailabilityStatus getAvailabilityStatus(Transaction transaction, Product product)
        {
            if (product == null || transaction == null)
            {
                return AvailabilityStatus.notAvailable(0);
            }

            int totalQuantity = 0;
            foreach (var transactionDetails in transaction.TransactionDetails)
            {
                if (transactionDetails.Quantity <= 0)
                {
                    throw new IncorrectProductQuantityException(
                        "Quantity of product in transaction should be bigger than 0.");
                }

                if (transactionDetails.ProductId == product.Id)
                {
                    totalQuantity += transactionDetails.Quantity;
                }
            }

            int difference = product.Quantity.Value - totalQuantity;
            if (difference < 0)
            {
                return AvailabilityStatus.notAvailable(product.Quantity.Value);
            }

            return AvailabilityStatus.available(product.Quantity.Value);
        }
    }
}