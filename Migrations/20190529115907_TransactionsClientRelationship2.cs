﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ownthatshop.Migrations
{
    public partial class TransactionsClientRelationship2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Clients_ClientId1",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_ClientId1",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "ClientId1",
                table: "Transactions");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ClientId1",
                table: "Transactions",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_ClientId1",
                table: "Transactions",
                column: "ClientId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Clients_ClientId1",
                table: "Transactions",
                column: "ClientId1",
                principalTable: "Clients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
