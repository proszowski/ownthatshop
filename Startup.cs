﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ownthatshop.Database;
using ownthatshop.Database.PostgreSql;
using ownthatshop.Model.Client;
using ownthatshop.Model.Product;
using ownthatshop.Model.Statistics;
using ownthatshop.Model.Transaction;
using ownthatshop.Service;

namespace ownthatshop
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddEntityFrameworkNpgsql().AddDbContext<OwnThatShopContext>(opt =>
                opt.UseNpgsql(Environment.GetEnvironmentVariable("OWN_THAT_SHOP_DB_CONNECTION")));
            services.AddTransient<ProductFacade>();
            services.AddTransient<ClientFacade>();
            services.AddTransient<StatisticsFacade>();
            services.AddTransient<TransactionFacade>();
            services.AddTransient<AvailabilityChecker>();
            services.AddTransient<IProductRepository, ProductDatabase>();
            services.AddTransient<IClientRepository, ClientDatabase>();
            services.AddTransient<IClientTypeRepository, ClientTypeDatabase>();
            services.AddTransient<ICompaniesRepository, CompaniesDatabase>();
            services.AddTransient<ITransactionRepository, TransactionDatabase>();
            services.AddCors(options =>
                options.AddPolicy("AllowSpecificOrigin",
                    builder => builder.WithOrigins("http://localhost:4200").WithMethods("GET", "POST", "DELETE", "PUT").WithHeaders("content-type"))
            );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            
            app.UseHttpsRedirection();
            app.UseMvc();
            app.UseCors("AllowSpecificOrigin");
        }
    }
}