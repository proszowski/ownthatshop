using System;
using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ownthatshop.Model;
using ownthatshop.Model.Product;
using ownthatshop.Model.Transaction;
using ownthatshop.Service;

namespace ownthatshop.Controllers
{
    [Route("api/transactions")]
    [ApiController]
    [EnableCors("AllowSpecificOrigin")]
    public class TransactionsController
    {
        private TransactionFacade _transactionFacade;
        private ProductFacade _productFacade;
        private AvailabilityChecker _availabilityChecker;

        public TransactionsController(TransactionFacade transactionFacade, AvailabilityChecker availabilityChecker, ProductFacade productFacade)
        {
            _transactionFacade = transactionFacade;
            _availabilityChecker = availabilityChecker;
            _productFacade = productFacade;
        }

        [HttpGet]
        public ActionResult<List<Transaction>> GetAll()
        {
            return _transactionFacade.GetAll();
        }

        [HttpPost("check-availability/{productId}")]
        public ObjectResult CheckAvailability([FromBody] Transaction transaction, int productId)
        {
            Product product = _productFacade.GetProduct(productId).Value;
            int? statusCode = (int?) HttpStatusCode.OK;
            AvailabilityStatus availabilityStatus = null;
            try
            {
                availabilityStatus = _availabilityChecker.getAvailabilityStatus(transaction, product);
            }
            catch (ModelException e)
            {
                statusCode = (int?) HttpStatusCode.BadRequest;
            }
            
            return new ObjectResult(availabilityStatus)
            {
                StatusCode = statusCode
            };
        }

        [HttpPost("add")]
        public ObjectResult AddTransaction(Transaction transaction)
        {
            try
            {
                _transactionFacade.Add(transaction);
            }
            catch (ModelException e)
            {
                return new ObjectResult(e.Message)
                {
                    StatusCode = (int?) HttpStatusCode.BadRequest
                };
            }

            return new ObjectResult("OK")
            {
                StatusCode = (int?) HttpStatusCode.OK
            };
        }
    }
}