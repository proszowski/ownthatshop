using System;
using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ownthatshop.Model;
using ownthatshop.Model.Client;

namespace ownthatshop.Controllers
{
    [Route("api/clients")]
    [ApiController]
    [EnableCors("AllowSpecificOrigin")]
    public class ClientsController
    {

        private ClientFacade _clientFacade;

        public ClientsController(ClientFacade clientFacade)
        {
            _clientFacade = clientFacade;
        }
        
        [HttpGet]
        public ActionResult<List<Client>> GetAllClients()
        {
            return _clientFacade.GetAllClients();
        }
        
        [HttpGet("{id}", Name = "GetClient")]
        public ActionResult<Client> GetClientWithId(int id)
        {
            return _clientFacade.GetClientWithId(id);
        }
        
        [HttpPost("add")]
        public ObjectResult AddClient(Client client)
        {
            try
            {
                _clientFacade.AddClient(client);
                return new ObjectResult("OK");
            }
            catch (ModelException e)
            {
                return new ObjectResult(e.Message)
                {
                    StatusCode = (int?) HttpStatusCode.BadRequest
                };
            }
        }

        [HttpPut("update")]
        public ObjectResult UpdateClient(Client client)
        {
            try
            {
                _clientFacade.UpdateClient(client);
                return new ObjectResult("OK");
            }
            catch (ModelException e)
            {
                return new ObjectResult(e.Message)
                {
                    StatusCode = (int?) HttpStatusCode.BadRequest
                };
            }
        }

        [HttpDelete("delete/{id}", Name = "DeleteClient")]
        public void RemoveClient(int id)
        {
            _clientFacade.DeleteClient(id);
        }
        
        [HttpGet("types")]
        public ActionResult<List<ClientType>> GetAllClientTypes()
        {
            return _clientFacade.GetAllClientTypes();
        }
        
        [HttpGet("companies")]
        public ActionResult<List<Company>> GetAllCompanies()
        {
            return _clientFacade.GetAllCompanies();
        }
    }
}