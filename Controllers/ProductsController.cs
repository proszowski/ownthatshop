using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ownthatshop.Model;
using ownthatshop.Model.Product;

namespace ownthatshop.Controllers
{
    [Route("api/products")]
    [ApiController]
    [EnableCors("AllowSpecificOrigin")]
    public class ProductsController
    {
        private readonly ProductFacade _productFacade;

        public ProductsController(ProductFacade productFacade)
        {
            _productFacade = productFacade;
        }

        [HttpGet]
        public ActionResult<List<Product>> GetAllProducts(int page = 0)
        {
            var products = _productFacade.GetAllProducts();
            if (page != 0)
            {
                var products_per_page = 10;
                products = products.Value.GetRange(page * products_per_page,
                    products_per_page);
            }

            return products;
        }

        [HttpGet("{id}", Name = "GetProduct")]
        public ActionResult<Product> GetProduct(int id)
        {
            return _productFacade.GetProduct(id);
        }

        [HttpPut("update")]
        public ObjectResult UpdateProduct([FromBody] Product product)
        {
            try
            {
                _productFacade.UpdateProduct(product);
                return new ObjectResult("OK");
            }
            catch (ModelException e)
            {
                return new ObjectResult(e.Message)
                {
                    StatusCode = (int?) HttpStatusCode.BadRequest,
                };
            }
        }

        [HttpPost("add")]
        public ObjectResult AddProduct(Product product)
        {
            try
            {
                _productFacade.AddProduct(product);
                return new ObjectResult("OK");
            }
            catch (ModelException e)
            {
                return new ObjectResult(e.Message)
                {
                    StatusCode = (int?) HttpStatusCode.BadRequest
                };
            }
        }

        [HttpDelete("delete/{id}")]
        public void DeleteProduct(long id)
        {
            _productFacade.RemoveProduct(id);
        }

        [HttpGet("available-types")]
        public ActionResult<ICollection<ProductType>> GetAvailableTypes()
        {
            return _productFacade.GetAvailableTypes();
        }
    }
}