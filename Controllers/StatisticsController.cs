using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ownthatshop.Model.Statistics;
using ownthatshop.Model.Statistics.Filter;
using ownthatshop.Model.Statistics.Query;

namespace ownthatshop.Controllers
{
    [Route("api/statistics")]
    [ApiController]
    [EnableCors("AllowSpecificOrigin")]
    public class StatisticsController
    {
        private StatisticsFacade _statisticsFacade;

        public StatisticsController(StatisticsFacade statisticsFacade)
        {
            _statisticsFacade = statisticsFacade;
        }

        [HttpPost("{type}")]
        public ActionResult<Statistic> GetStatistics([FromBody] QueryDto queryDto, ResultType type)
        {
            return _statisticsFacade.GetStatistics(QueryBinder.bind(queryDto), type);
        }
    }
}