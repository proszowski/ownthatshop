using ownthatshop.Model.Client.Exception;

namespace ownthatshop.Model.Client
{
    public class ClientType
    {
        public int? Id { get; set; }
        public string Name { get; set; }

        public void Validate()
        {
            if (string.IsNullOrEmpty(Name))
            {
                throw new ClientTypeNameCannotBeEmptyException("Client type name cannot be empty nor null");
            }
        }
    }
}