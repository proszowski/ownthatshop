using ownthatshop.Model.Client.Exception;

namespace ownthatshop.Model.Client
{
    public class Company
    {
        public long? Id { get; set; }
        public string Name { get; set; }

        public Company()
        {
            
        }

        public Company(long id, string name)
        {
            Id = id;
            Name = name;
        }

        public void Validate()
        {
            if (string.IsNullOrEmpty(Name))
            {
                throw new CompanyNameCannotBeNullException("Company name cannot be null nor empty");
            }
        }
    }
}