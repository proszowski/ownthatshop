using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ownthatshop.Model.Client.Exception;

namespace ownthatshop.Model.Client
{
    public class ClientFacade
    {
        
        private readonly IClientRepository _clientRepository;
        private readonly IClientTypeRepository _clientTypesRepository;
        private readonly ICompaniesRepository _companiesRepository;

        public ClientFacade(IClientRepository clientRepository, IClientTypeRepository clientTypeRepository, ICompaniesRepository companiesRepository)
        {
            _clientRepository = clientRepository;
            _clientTypesRepository = clientTypeRepository;
            _companiesRepository = companiesRepository;
        }
    
        public ActionResult<List<Client>> GetAllClients()
        {
            return _clientRepository.GetAll();
        }

        public ActionResult<Client> GetClientWithId(long id)
        {
            return _clientRepository.GetAll().Value.Find(client => client.Id.Equals(id));
        }

        public void AddClient(Client client)
        {
            Validate(client);
            client.Address.Id = null;
            client.Company.Id = null;
            client.Contact.Id = null;
            client.Id = null;
            _clientRepository.Add(client);
        }

        public void UpdateClient(Client client)
        {
            Validate(client);
            Client existingClient = _clientRepository.GetById(client.Id); 
            if (existingClient == null)
            {
                throw new ClientDoesNotExistException("Client with given id does not exist");
            }

            client.Address.Id = existingClient.Address.Id;
            client.Company.Id = existingClient.Company.Id;
            client.Contact.Id = existingClient.Contact.Id;
            _clientRepository.Update(client);
        }

        public void DeleteClient(int id)
        {
            _clientRepository.Delete(id);
        }

        public ActionResult<List<ClientType>> GetAllClientTypes()
        {
            return _clientTypesRepository.GetAllTypes();
        }

        public ActionResult<List<Company>> GetAllCompanies()
        {
            return _companiesRepository.GetAllCompanies();
        }
        
        private void Validate(Client client)
        {
            if (client == null)
            {
                throw new ClientCannotBeNullException("Client cannot be null");
            }
            
            client.Validate();

            if (_clientTypesRepository.FindById(client.ClientType.Id) == null)
            {
                throw new ClientTypeDoesNotExistException("Client type with given id  does not exist");
            }
        }
    }
}