using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace ownthatshop.Model.Client
{
    public interface IClientRepository
    {
        ActionResult<List<Client>> GetAll();
        void Add(Client client);
        void Update(Client client);
        Client GetById(long? clientId);
        void Delete(int client);
    }
}