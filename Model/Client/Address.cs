using Microsoft.EntityFrameworkCore.Query.ExpressionTranslators;
using ownthatshop.Model.Client.Exception;

namespace ownthatshop.Model.Client
{
    public class Address
    {
        public long? Id { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string PostCode { get; set; }
        public int? HouseNumber { get; set; }

        public void Validate()
        {
            if (string.IsNullOrEmpty(Street))
            {
                throw new IncorrectStreetException("Street cannot be null nor empty");
            }
            if (Street.Length > 30)
            {
                throw new IncorrectStreetException("Street cannot be longer than 30 characters");
            }

            if (string.IsNullOrEmpty(City))
            {
                throw new IncorrectCityException("City cannot be null nor empty");
            }

            if (City.Length > 30)
            {
                throw new IncorrectCityException("City cannot be longer than 30 characters");
            }

            if (string.IsNullOrEmpty(Country))
            {
                throw new IncorrectCountryException("Country cannot be null nor empty");
            }

            if (Country.Length > 30)
            {
                throw new IncorrectCountryException("Country cannot be longer than 30 characters");
            }

            if (HouseNumber == null)
            {
                throw new IncorrectHouseNumberException("House number cannot be null nor empty");
            }
        }
    }
}