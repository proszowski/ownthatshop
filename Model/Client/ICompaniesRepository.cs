using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace ownthatshop.Model.Client
{
    public interface ICompaniesRepository
    {
        ActionResult<List<Company>> GetAllCompanies();
        void Add(Company company);
    }
}