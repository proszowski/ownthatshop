using ownthatshop.Model.Client.Exception;

namespace ownthatshop.Model.Client
{
    public class Contact
    {
        public long? Id { get; set; }
        public long? PhoneNumber{get; set; }
        public string Email { get; set; }

        public void Validate()
        {
            if (PhoneNumber == null || PhoneNumber.ToString().Length != 9)
            {
                throw new IncorrectPhoneNumberException("Phone number has to have 9 digits");
            }

            if (Email == null || !Email.Contains("@"))
            {
                throw new IncorrectEmailException("Incorrect email");
            }
        }
    }
}