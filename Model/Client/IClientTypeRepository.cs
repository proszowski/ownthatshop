using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ownthatshop.Model.Product;

namespace ownthatshop.Model.Client
{
    public interface IClientTypeRepository
    {
        ActionResult<List<ClientType>> GetAllTypes();
        void Add(ClientType clientClientType);
        ClientType FindById(int? clientTypeId);
    }
}