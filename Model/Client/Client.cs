using ownthatshop.Model.Client.Exception;

namespace ownthatshop.Model.Client
{
    public class Client
    {
        public Client(long? id, string firstName, string surname, ClientType clientType, Contact contact,
            Address address, Company company)
        {
            Id = id;
            FirstName = firstName;
            Surname = surname;
            ClientType = clientType;
            Contact = contact;
            Address = address;
            Company = company;
        }

        public Client()
        {
        }

        public Client(ClientBuilder builder)
        {
            Id = builder.Id;
            FirstName = builder.FirstName;
            Surname = builder.Surname;
            ClientType = builder.ClientType;
            Contact = builder.Contact;
            Address = builder.Address;
            Company = builder.Company;
        }

        public long? Id { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }

        public Company Company { get; set; }
        public ClientType ClientType { get; set; }
        public Contact Contact { get; set; }
        public Address Address { get; set; }

        public void Validate()
        {
            if (string.IsNullOrEmpty(FirstName))
            {
                throw new FirstNameCannotBeNullException("First name cannot be null nor empty.");
            }

            if (FirstName.Length > 25)
            {
                throw new FirstNameCannotBeNullException("First name cannot be longer than 25 characters.");
            }

            if (string.IsNullOrEmpty(Surname))
            {
                throw new SurnameCannotBeNullException("Surname cannot be null nor empty.");
            }

            if (Surname.Length > 25)
            {
                throw new SurnameCannotBeNullException("Surname cannot be longer than 25 characters.");
            }

            if (Company == null)
            {
                throw new CompanyCannotBeNullException("Company cannot be null.");
            }

            if (ClientType == null)
            {
                throw new ClientTypeCannotBeNullException("Client type cannot be null.");
            }

            if (Contact == null)
            {
                throw new ContactCannotBeNullException("Contact cannot be null.");
            }

            if (Address == null)
            {
                throw new AddressCannotBeNullException("Address cannot be null.");
            }

            Company.Validate();
            ClientType.Validate();
            Contact.Validate();
            Address.Validate();
        }
    }
}