using ownthatshop.Model.Client;

namespace ownthatshop.Model.Client
{
    public class ClientBuilder
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public ClientType ClientType { get; set; }
        public Contact Contact { get; set; }
        public Address Address { get; set; }
        public Company Company { get; set; }

        private ClientBuilder() {
        }

        public static ClientBuilder AClientDto()
        {
            return new ClientBuilder();
        }

        public ClientBuilder WithId(long id)
        {
            Id = id;
            return this;
        }
        
        public ClientBuilder WithFirstName(string firstName)
        {
            FirstName = firstName;
            return this;
        }
        
        public ClientBuilder WithSurname(string surname)
        {
            Surname = surname;
            return this;
        }
        
        public ClientBuilder WithClientType(ClientType clientType)
        {
            ClientType = clientType;
            return this;
        }
        
        public ClientBuilder WithContact(Contact contact)
        {
            Contact = contact;
            return this;
        }
        
        public ClientBuilder WithAddress(Address address)
        {
            Address = address;
            return this;
        }
        
        public ClientBuilder WithCompany(Company company)
        {
            Company = company;
            return this;
        }


        public Client Build()
        {
            return new Client(this);
        }
    }
    
}