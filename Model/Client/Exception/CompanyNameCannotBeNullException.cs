namespace ownthatshop.Model.Client.Exception
{
    public class CompanyNameCannotBeNullException : ModelException
    {
        public CompanyNameCannotBeNullException(string message) : base(message)
        {
        }
    }
}