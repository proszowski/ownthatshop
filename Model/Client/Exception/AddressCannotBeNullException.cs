namespace ownthatshop.Model.Client.Exception
{
    public class AddressCannotBeNullException : ModelException
    {
        public AddressCannotBeNullException(string message) : base(message)
        {
        }
    }
}