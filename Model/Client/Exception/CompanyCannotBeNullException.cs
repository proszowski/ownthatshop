namespace ownthatshop.Model.Client.Exception
{
    public class CompanyCannotBeNullException : ModelException
    {
        public CompanyCannotBeNullException(string message) : base(message)
        {
        }
    }
}