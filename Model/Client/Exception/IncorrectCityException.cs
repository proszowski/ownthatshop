namespace ownthatshop.Model.Client.Exception
{
    public class IncorrectCityException : ModelException
    {
        public IncorrectCityException(string message) : base(message)
        {
        }
    }
}