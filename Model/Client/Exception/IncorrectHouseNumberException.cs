namespace ownthatshop.Model.Client.Exception
{
    public class IncorrectHouseNumberException : ModelException
    {
        public IncorrectHouseNumberException(string message) : base(message)
        {
        }
    }
}