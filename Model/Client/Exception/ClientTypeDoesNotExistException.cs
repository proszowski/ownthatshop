namespace ownthatshop.Model.Client.Exception
{
    public class ClientTypeDoesNotExistException : ModelException
    {
        public ClientTypeDoesNotExistException(string message) : base(message)
        {
        }
    }
}