namespace ownthatshop.Model.Client.Exception
{
    public class ClientTypeNameCannotBeEmptyException : ModelException
    {
        public ClientTypeNameCannotBeEmptyException(string message) : base(message)
        {
        }
    }
}