namespace ownthatshop.Model.Client.Exception
{
    public class FirstNameCannotBeNullException : ModelException
    {
        public FirstNameCannotBeNullException(string message) : base(message)
        {
        }
    }
}