namespace ownthatshop.Model.Client.Exception
{
    public class ContactCannotBeNullException : ModelException
    {
        public ContactCannotBeNullException(string message) : base(message)
        {
        }
    }
}