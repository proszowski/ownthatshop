namespace ownthatshop.Model.Client.Exception
{
    public class IncorrectCountryException : ModelException
    {
        public IncorrectCountryException(string message) : base(message)
        {
        }
    }
}