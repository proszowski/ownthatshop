namespace ownthatshop.Model.Client.Exception
{
    public class ClientTypeCannotBeNullException : ModelException
    {
        public ClientTypeCannotBeNullException(string message) : base(message)
        {
        }
    }
}