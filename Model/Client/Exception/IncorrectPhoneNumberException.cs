namespace ownthatshop.Model.Client.Exception
{
    public class IncorrectPhoneNumberException : ModelException
    {
        public IncorrectPhoneNumberException(string message) : base(message)
        {
        }
    }
}