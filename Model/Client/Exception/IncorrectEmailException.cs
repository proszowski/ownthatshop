namespace ownthatshop.Model.Client.Exception
{
    public class IncorrectEmailException : ModelException
    {
        public IncorrectEmailException(string message) : base(message)
        {
        }
    }
}