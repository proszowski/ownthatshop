namespace ownthatshop.Model.Client.Exception
{
    public class SurnameCannotBeNullException : ModelException
    {
        public SurnameCannotBeNullException(string message) : base(message)
        {
        }
    }
}