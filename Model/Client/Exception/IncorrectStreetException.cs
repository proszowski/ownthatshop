namespace ownthatshop.Model.Client.Exception
{
    public class IncorrectStreetException : ModelException    
    {
        public IncorrectStreetException(string message) : base(message)
        {
        }
    }
}