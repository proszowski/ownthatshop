namespace ownthatshop.Model.Client.Exception
{
    public class ClientDoesNotExistException : ModelException
    {
        public ClientDoesNotExistException(string message) : base(message)
        {
        }
    }
}