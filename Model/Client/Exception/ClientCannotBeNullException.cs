namespace ownthatshop.Model.Client.Exception
{
    public class ClientCannotBeNullException : ModelException
    {
        public ClientCannotBeNullException(string message) : base(message)
        {
        }
    }
}