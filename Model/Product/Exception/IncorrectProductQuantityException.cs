namespace ownthatshop.Model.Product.Exception
{
    public class IncorrectProductQuantityException : ModelException
    {
        public IncorrectProductQuantityException(string message) : base(message)
        {
        }
    }
}