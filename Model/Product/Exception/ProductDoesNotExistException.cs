namespace ownthatshop.Model.Product.Exception
{
    public class ProductDoesNotExistException : ModelException
    {
        public ProductDoesNotExistException(string message) : base(message)
        {
        }
    }
}