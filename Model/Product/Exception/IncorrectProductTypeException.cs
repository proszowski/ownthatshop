namespace ownthatshop.Model.Product.Exception
{
    public class IncorrectProductTypeException : ModelException
    {
        public IncorrectProductTypeException(string message) : base(message)
        {
        }
    }
}