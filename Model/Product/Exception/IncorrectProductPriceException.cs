namespace ownthatshop.Model.Product.Exception
{
    public class IncorrectProductPriceException : ModelException
    {
        public IncorrectProductPriceException(string message) : base(message)
        {
        }
    }
}