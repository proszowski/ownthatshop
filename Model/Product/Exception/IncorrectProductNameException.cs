namespace ownthatshop.Model.Product.Exception
{
    public class IncorrectProductNameException: ModelException
    {
        public IncorrectProductNameException(string message) : base(message)
        {
        }
    }
}