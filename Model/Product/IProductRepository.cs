using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace ownthatshop.Model.Product
{
    public interface IProductRepository
    {
        ActionResult<List<Product>> FindAll();
        ActionResult<Product> FindById(long id);
        void SaveProduct(Product product);
        void RemoveProduct(long id);
        void UpdateProduct(Product product);
        ActionResult<ICollection<ProductType>> GetAvailableTypes();
    }
}