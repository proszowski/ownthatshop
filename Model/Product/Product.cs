
using ownthatshop.Model.Product.Dto;

namespace ownthatshop.Model.Product
{
    public class Product
    {
        public long? Id { get; set; }
        public string Name { get; set; }
        public decimal? Price { get; set; }
        public int? Quantity { get; set; }
        public string Image { get; set; }
        public ProductType ProductType { get; set; }

        public Product()
        {
        }

        public Product(ProductBuilder productBuilder)
        {
            Id = productBuilder.Id;
            Name = productBuilder.Name;
            Price = productBuilder.Price;
            Quantity = productBuilder.Quantity;
            Image = productBuilder.Image;
            ProductType = productBuilder.ProductType;
        }
    }
}