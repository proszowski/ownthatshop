using System;

namespace ownthatshop.Model.Product.Dto
{
    public class ProductBuilder
    {
        public long Id { get; private set; }
        public string Name { get; private set; }
        public decimal Price { get; private set; }
        public int Quantity { get; private set; }
        public string Image { get; private set; }
        public ProductType ProductType { get; private set; }

        private ProductBuilder()
        {
        }

        public static ProductBuilder aProductDto()
        {
            return new ProductBuilder();
        }

        public ProductBuilder withId(long id)
        {
            this.Id = id;
            return this;
        }
        
        public ProductBuilder withName(string name)
        {
            this.Name = name;
            return this;
        }
        
        public ProductBuilder withPrice(decimal price)
        {
            this.Price = price;
            return this;
        }
        
        
        public ProductBuilder withQuantity(int quantity)
        {
            this.Quantity = quantity;
            return this;
        }
        
        public ProductBuilder withImage(string image)
        {
            this.Image = image;
            return this;
        }
        
        public ProductBuilder withType(ProductType productType)
        {
            this.ProductType = productType;
            return this;
        }

        public Product build()
        {
            return new Product(this);
        }
    }
}