using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ownthatshop.Model.Product.Exception;

namespace ownthatshop.Model.Product
{
    public class ProductFacade
    {
        private IProductRepository _productRepository;

        public ProductFacade(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public ActionResult<List<Product>> GetAllProducts()
        {
            return _productRepository.FindAll();
        }

        public ActionResult<Product> GetProduct(long id)
        {
            return _productRepository.FindById(id);
        }

        public void RemoveProduct(long id)
        {
            _productRepository.RemoveProduct(id);
        }

        public void AddProduct(Product product)
        {
            Validate(product);
            _productRepository.SaveProduct(product);
        }

        public void UpdateProduct(Product product)
        {
            Validate(product);
            if (product.Id == null || _productRepository.FindById(product.Id.Value) == null)
            {
                throw new ProductDoesNotExistException("Product with given id does not exist");
            }

            product.ProductType = _productRepository.GetAvailableTypes().Value
                .FirstOrDefault(p => p.Id == product.ProductType.Id);
            _productRepository.UpdateProduct(product);
        }

        public ActionResult<ICollection<ProductType>> GetAvailableTypes()
        {
            return _productRepository.GetAvailableTypes();
        }


        private void Validate(Product product)
        {
            if (product.Name == null || product.Name.Length < 5)
            {
                throw new IncorrectProductNameException("Product name need to have at least 5 characters.");
            }

            if (product.Price == null || product.Price < 0 || product.Price != Math.Round(product.Price.Value, 2))
            {
                throw new IncorrectProductPriceException("Price must be decimal number with max two digits after dot");
            }

            if (product.Quantity == null || product.Quantity < 0)
            {
                throw new IncorrectProductQuantityException("Quantity should be a positive number");
            }

            if (product.ProductType == null || _productRepository.GetAvailableTypes().Value
                    .FirstOrDefault(pt => pt.Id == product.ProductType.Id) == null)
            {
                throw new IncorrectProductTypeException("Given type does not exist");
            }
        }
    }
}