using System;

namespace ownthatshop.Model
{
    public class ModelException : Exception
    {
        protected ModelException(string message): base(message)
        {
        }
    }
}