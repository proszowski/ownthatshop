using ownthatshop.Model.Product.Exception;

namespace ownthatshop.Model.Transaction
{
    public class TransactionDetails
    {
        public long Id { get; set; }
        public long ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal TotalPrice { get; set; }
        public long TransactionId { get; set; }

        public void Validate()
        {
            if (Quantity <= 0)
            {
                throw new IncorrectProductQuantityException(
                    "Quantity of product in transaction should be bigger than 0.");
            }
        }
    }
}