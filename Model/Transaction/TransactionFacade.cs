using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ownthatshop.Model.Client;
using ownthatshop.Model.Client.Exception;
using ownthatshop.Model.Product;
using ownthatshop.Model.Transaction.Exception;

namespace ownthatshop.Model.Transaction
{
    public class TransactionFacade
    {
        private readonly ITransactionRepository _transactionRepository;
        private readonly ProductFacade _productFacade;
        private readonly ClientFacade _clientFacade;

        public TransactionFacade(ITransactionRepository transactionRepository, ProductFacade productFacade, ClientFacade clientFacade)
        {
            _transactionRepository = transactionRepository;
            _productFacade = productFacade;
            _clientFacade = clientFacade;
        }

        public ActionResult<List<Transaction>> GetAll()
        {
            return _transactionRepository.FindAll();
        }

        public void Add(Transaction transaction)
        {
            if (transaction == null)
            {
                throw new TransactionCannotBeNullException("Transaction cannot be null");
            }
            
            ValidateClient(transaction);
            transaction.Validate();
            transaction.Date = DateTime.Now;
            
            var products = new List<Product.Product>();
            decimal price = 0;
            foreach (var transactionDetails in transaction.TransactionDetails)
            {
                var product = _productFacade.GetProduct(transactionDetails.ProductId).Value;
                transactionDetails.TotalPrice = transactionDetails.Quantity * product.Price.Value;
                price += transactionDetails.TotalPrice;
                var quantityAfterChange = product.Quantity = product.Quantity - transactionDetails.Quantity;
                if (quantityAfterChange < 0)
                {
                    throw new ProductNotAvailableException("Product with id: " + product.Id + " is out of stock");
                }
                products.Add(product);
            }

            foreach (var product in products)
            {
                _productFacade.UpdateProduct(product);
            }

            transaction.TotalPrice = price;

            _transactionRepository.Add(transaction);
        }

        private void ValidateClient(Transaction transaction)
        {
            Client.Client client = _clientFacade.GetClientWithId(transaction.ClientId).Value;
            if (client == null)
            {
                throw new ClientDoesNotExistException("Client does not exist");
            }
        }
    }
}