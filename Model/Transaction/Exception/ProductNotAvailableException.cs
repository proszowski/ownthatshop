using System;

namespace ownthatshop.Model.Transaction.Exception
{
    public class ProductNotAvailableException : ModelException 
    {
        public ProductNotAvailableException(string message) : base(message)
        {
        }
    }
}