namespace ownthatshop.Model.Transaction.Exception
{
    public class TransactionCannotBeNullException : ModelException
    {
        public TransactionCannotBeNullException(string message) : base(message)
        {
        }
    }
}