namespace ownthatshop.Model.Transaction
{
    public class TransactionDetailsCannotBeEmptyNorNullException: ModelException
    {
        public TransactionDetailsCannotBeEmptyNorNullException(string message) : base(message)
        {
        }
    }
}