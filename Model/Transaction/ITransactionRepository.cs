using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace ownthatshop.Model.Transaction
{
    public interface ITransactionRepository
    {
        ActionResult<List<Transaction>> FindAll();
        void Add(Transaction transaction);
    }
}