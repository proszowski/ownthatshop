using System;
using System.Collections.Generic;
using ownthatshop.Model.Client.Exception;

namespace ownthatshop.Model.Transaction
{
    public class Transaction
    {
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public decimal TotalPrice { get; set; }
        public long ClientId { get; set; }
        public ICollection<TransactionDetails> TransactionDetails { get; set; }

        public override string ToString()
        {
            return "Transaction{ id: " + Id + ", date: " + Date + ", totalPrice: " + TotalPrice + ", clientId: " + ClientId + ", transaction details: " + TransactionDetails + "}";
        }

        public void Validate()
        {
            if (TransactionDetails == null || TransactionDetails.Count == 0)
            {
                throw new TransactionDetailsCannotBeEmptyNorNullException("Transaction details cannot be empty nor null");
            }

            foreach (var transactionDetails in TransactionDetails)
            {
                transactionDetails.Validate();
            }
        }
    }
    
}