using System;
using System.Collections.Generic;

namespace ownthatshop.Model.Statistics.Filter
{
    public class FilterableList<T>
    {
        public List<Filterable> Filterables { get; }
        public FilterableList(ICollection<T> objects)
        {
            Filterables = new List<Filterable>();
            foreach (var o in objects)
            {
                Filterables.Add(new Filterable(o));
            }
        }
    }
}