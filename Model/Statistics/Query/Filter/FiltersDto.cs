using System.Collections.Generic;

namespace ownthatshop.Model.Statistics.Filter
{
    public class FiltersDto
    {
        public Dictionary<FilterName, FilterConfiguration> FilterConfigurations { get; set; }
    }
}