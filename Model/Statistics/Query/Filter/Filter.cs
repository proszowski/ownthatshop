using System;
using System.Collections.Generic;

namespace ownthatshop.Model.Statistics.Filter
{
    public abstract class Filter
    {
        protected FilterConfiguration FilterConfiguration;
        
        protected Filter(FilterConfiguration filterConfiguration)
        {
            FilterConfiguration = filterConfiguration;
        }

        public abstract Boolean Apply(Filterable filterable);
    }
}