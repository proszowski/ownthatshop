using System.Collections.Generic;

namespace ownthatshop.Model.Statistics.Filter
{
    public class FilterConfiguration
    {
        public Dictionary<string, string> Parameters { get; set; }
    }
}