using System.Collections.Generic;

namespace ownthatshop.Model.Statistics.Filter
{
    public class FiltersUnpacker
    {
        public static Filters UnpackDto(FiltersDto filtersDto)
        {
            List<Filter> filters = new List<Filter>();
            
            if (filtersDto.FilterConfigurations == null)
            {
                return new Filters(filters);
            }
            
            foreach (var filterConfiguration in filtersDto.FilterConfigurations)
            {
                switch (filterConfiguration.Key)
                {
                    case FilterName.Country:
                        filters.Add(new CountryFilter(filterConfiguration.Value));
                        break;
                    case FilterName.ClientType:
                        filters.Add(new ClientTypeFilter(filterConfiguration.Value));
                        break;
                    case FilterName.DateRange:
                        filters.Add(new DateRangeFilter(filterConfiguration.Value));
                        break;
                    case FilterName.PriceRange:
                        filters.Add(new PriceRangeFilter(filterConfiguration.Value));
                        break;
                    case FilterName.Product:
                        filters.Add(new ProductFilter(filterConfiguration.Value));
                        break;
                    case FilterName.Client:
                        filters.Add(new ClientFilter(filterConfiguration.Value));
                        break;
                }
            }

            return new Filters(filters);
        }
    }
}