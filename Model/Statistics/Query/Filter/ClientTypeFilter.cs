using System;
using ownthatshop.Model.Client;

namespace ownthatshop.Model.Statistics.Filter
{
    public class ClientTypeFilter : Filter
    {
        private string _clientTypeName;
        
        public ClientTypeFilter(FilterConfiguration filterConfiguration) : base(filterConfiguration)
        {
            if (filterConfiguration.Parameters.ContainsKey("name"))
            {
            _clientTypeName = filterConfiguration.Parameters["name"];
            }
            else
            {
                _clientTypeName = "";
            }
        }

        public override bool Apply(Filterable filterable)
        {
            if (filterable.Entity.GetType() == typeof(Client.Client))
            {
                var client = (Client.Client) filterable.Entity;
                return client.ClientType.Name.ToLower().Equals(_clientTypeName.ToLower());
            }

            return true;
        }
    }
}