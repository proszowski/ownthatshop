namespace ownthatshop.Model.Statistics.Filter
{
    public class ClientFilter : Filter
    {
        private long? Id;
        
        public ClientFilter(FilterConfiguration filterConfiguration) : base(filterConfiguration)
        {
            if (filterConfiguration.Parameters.ContainsKey("id"))
            {
                Id = long.Parse(filterConfiguration.Parameters["id"]);
            }
        }

        public override bool Apply(Filterable filterable)
        {
            if (Id == null)
            {
                return true;
            }

            if (filterable.Entity.GetType() == typeof(Client.Client))
            {
                var client = (Client.Client) filterable.Entity;
                return client.Id == Id;
            }

            return true;
        }
    }
}