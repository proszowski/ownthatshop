using System;

namespace ownthatshop.Model.Statistics.Filter
{
    public class DateRangeFilter : Filter
    {
        private Range<DateTime> Range;
        public DateRangeFilter(FilterConfiguration filterConfiguration) : base(filterConfiguration)
        {
            DateTime start;
            DateTime end;
            if (filterConfiguration.Parameters.ContainsKey("start"))
            {
                 start = DateTime.Parse(filterConfiguration.Parameters["start"]);
            }
            else
            {
                start = DateTime.MinValue;
            }
            
            if (filterConfiguration.Parameters.ContainsKey("end"))
            {
                end = DateTime.Parse(filterConfiguration.Parameters["end"]);
            }
            else
            {
                end = DateTime.MaxValue;
            }
            Range = new Range<DateTime>(start, end);
        }

        public override bool Apply(Filterable filterable)
        {
            if (filterable.Entity.GetType() == typeof(Transaction.Transaction))
            {
                var transaction = (Transaction.Transaction) filterable.Entity;
                return Range.Includes(transaction.Date);
            }

            return true;
        }
    }
}