
using System;

namespace ownthatshop.Model.Statistics.Filter
{
    public class Filterable
    {
        public Object Entity { get; }

        public Filterable(Object entity)
        {
            Entity = entity;
        }
    }
}