using System;

namespace ownthatshop.Model.Statistics.Filter
{
    public class PriceRangeFilter : Filter
    {
        private Range<decimal> PriceRange;
        
        public PriceRangeFilter(FilterConfiguration filterConfiguration) : base(filterConfiguration)
        {
            decimal start = Decimal.MinValue;
            decimal end = Decimal.MaxValue;
            
            if (filterConfiguration.Parameters.ContainsKey("start"))
            {
                start = decimal.Parse(filterConfiguration.Parameters["start"]);
            }
            if (filterConfiguration.Parameters.ContainsKey("end"))
            {
                end = decimal.Parse(filterConfiguration.Parameters["end"]);
            }
            
            PriceRange = new Range<decimal>(start, end);
        }

        public override bool Apply(Filterable filterable)
        {
            if (filterable.Entity.GetType() == typeof(Product.Product))
            {
                var product = (Product.Product) filterable.Entity;
                return PriceRange.Includes(product.Price ?? 0);
            }

            return true;
        }
    }
}