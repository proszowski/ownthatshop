using System;
using System.Collections.Generic;

namespace ownthatshop.Model.Statistics.Filter
{
    public class Filters
    {
        private List<Filter> FiltersList { get; }

        public Filters()
        {
            FiltersList = new List<Filter>();
        }

        public Filters(List<Filter> filtersList)
        {
            FiltersList = filtersList ?? new List<Filter>();
        }

        public List<Filterable> Apply(List<Filterable> filterables)
        {
            var filteredList = new List<Filterable>(filterables);
            filterables.ForEach(
                filterable => FiltersList.ForEach(
                    filter =>
                    {
                        if (!filter.Apply(filterable))
                        {
                            filteredList.Remove(filterable);
                        }
                    })
                );

            return filteredList;
        }
    }
}