namespace ownthatshop.Model.Statistics.Filter
{
    public class CountryFilter : Filter
    {
        private readonly string _countryName;

        public CountryFilter(FilterConfiguration filterConfiguration) : base(filterConfiguration)
        {
            if (filterConfiguration.Parameters.ContainsKey("countryName"))
            {
            _countryName = filterConfiguration.Parameters["countryName"];
            }
            else
            {
                _countryName = "";
            }
        }

        public override bool Apply(Filterable filterable)
        {
            if (filterable.Entity.GetType() == typeof(Client.Client))
            {
                var client = (Client.Client) filterable.Entity;
                return client.Address.Country.ToLower().Equals(_countryName.ToLower());
            }

            return true;
        }
    }
}