namespace ownthatshop.Model.Statistics.Filter
{
    public class ProductFilter : Filter
    {
        private long? Id;
        
        public ProductFilter(FilterConfiguration filterConfiguration) : base(filterConfiguration)
        {
            if (filterConfiguration.Parameters.ContainsKey("id"))
            {
                Id = long.Parse(filterConfiguration.Parameters["id"]);
            }
        }

        public override bool Apply(Filterable filterable)
        {
            if (Id == null)
            {
                return true;
            }
            
            if (filterable.Entity.GetType() == typeof(Product.Product))
            {
                var product = (Product.Product) filterable.Entity;
                return product.Id == Id;
            }

            return true;
        }
    }
}