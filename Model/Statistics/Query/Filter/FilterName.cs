namespace ownthatshop.Model.Statistics.Filter
{
    public enum FilterName
    {
        Country,
        ClientType,
        DateRange,
        PriceRange,
        Product,
        Client
    }
}