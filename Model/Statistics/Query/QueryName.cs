namespace ownthatshop.Model.Statistics.Query
{
    public enum QueryName
    {
        TheMostExpensiveProduct,
        TheLessExpensiveProduct,
        TheMostPopularProduct,
        TheLessPopularProduct,
        BoughtTheBiggestAmountOfProducts
    }
}