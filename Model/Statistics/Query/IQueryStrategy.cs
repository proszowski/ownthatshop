using System.Collections.Generic;
using ownthatshop.Model.Statistics.Filter;

namespace ownthatshop.Model.Statistics.Query
{
    public interface IQueryStrategy
    {
        List<Filterable> getQueryResult(List<Filterable> filterables, List<Filterable> otherFilterables);
    }
}