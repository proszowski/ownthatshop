using System.Collections.Generic;
using System.Linq;
using ownthatshop.Model.Statistics.Filter;

namespace ownthatshop.Model.Statistics.Query.Client
{
    public class BoughtTheBiggestAmountOfProductsQueryStrategy : IQueryStrategy
    {
        public List<Filterable> getQueryResult(List<Filterable> filterables, List<Filterable> transactions)
        {
            var queryResult = new BoughtTheSmallestAmountOfProducts().getQueryResult(filterables, transactions);
            queryResult.Reverse();
            return queryResult;
        }
    }
}