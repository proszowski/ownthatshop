using System.Collections.Generic;
using System.Linq;
using ownthatshop.Model.Statistics.Filter;

namespace ownthatshop.Model.Statistics.Query.Client
{
    public class BoughtTheSmallestAmountOfProducts : IQueryStrategy
    {
        public List<Filterable> getQueryResult(List<Filterable> filterables, List<Filterable> transactions)
        {
            return transactions
                .Select(e => (Transaction.Transaction) e.Entity)
                .Join(
                    filterables.Select(e => (Model.Client.Client) e.Entity),
                    t => t.ClientId,
                    c => c.Id,
                    (t, c) => new
                    {
                        Transaction = t,
                        Client = c
                    })
                .GroupBy(
                    row => row.Client,
                    row => row.Transaction.TransactionDetails.Select(td => td.Quantity).Sum(),
                    (c, q) => new
                    {
                        Client = c,
                        Quantity = q
                    }
                    )
                .Select(row => new Filterable(row.Client))
                .ToList();
        }
    }
}