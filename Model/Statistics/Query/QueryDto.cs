using ownthatshop.Model.Statistics.Filter;

namespace ownthatshop.Model.Statistics.Query
{
    public class QueryDto
    {
        public ResultType? ResultType { get; set; }
        public QueryName? QueryName { get; set; }
        public FiltersDto FiltersDto { get; set; }
    }
}