using System.Collections.Generic;
using System.Linq;
using ownthatshop.Model.Statistics.Filter;

namespace ownthatshop.Model.Statistics.Query.Product
{
    public class TheMostPopularProduct : IQueryStrategy
    {
        public List<Filterable> getQueryResult(List<Filterable> filterables, List<Filterable> otherFilterables)
        {
            List<Filterable> queryResult = new TheLessPopularProduct()
                .getQueryResult(filterables, otherFilterables);
            queryResult.Reverse();
            return queryResult;
        }
    }
}