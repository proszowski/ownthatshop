using System.Collections.Generic;
using System.Linq;
using ownthatshop.Model.Statistics.Filter;

namespace ownthatshop.Model.Statistics.Query.Product
{
    public class TheLessPopularProduct : IQueryStrategy
    {
        public List<Filterable> getQueryResult(List<Filterable> filterables, List<Filterable> otherFilterables)
        {
            return otherFilterables
                .Select(e => (Transaction.Transaction) e.Entity)
                .SelectMany(transaction => transaction.TransactionDetails)
                .Join(
                    filterables.Select(e => (Model.Product.Product) e.Entity),
                    td => td.ProductId,
                    p => p.Id,
                    (td, p) => new
                    {
                        td.Quantity,
                        Product = p
                    }
                )
                .GroupBy(
                    p => p.Product,
                    q => q.Quantity,
                    (p, q) => new
                    {
                        Product = p,
                        Quantity = q.Sum()
                    })
                .OrderBy(row => row.Quantity)
                .Select(row => new Filterable(row.Product))
                .ToList();
        }
    }
}