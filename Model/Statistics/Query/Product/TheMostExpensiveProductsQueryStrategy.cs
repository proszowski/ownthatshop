using System.Collections.Generic;
using System.Linq;
using ownthatshop.Model.Statistics.Filter;

namespace ownthatshop.Model.Statistics.Query.Product
{
    public class TheMostExpensiveProductsQueryStrategy: IQueryStrategy
    {
        public List<Filterable> getQueryResult(List<Filterable> filterablesProducts, List<Filterable> otherFilterables)
        {
            return filterablesProducts
                .Select(e => (Model.Product.Product) e.Entity)
                .OrderByDescending(p => p.Price)
                .Select(p => new Filterable(p))
                .ToList();
        }
    }
}