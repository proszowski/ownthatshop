using System.Collections.Generic;
using ownthatshop.Model.Statistics.Filter;

namespace ownthatshop.Model.Statistics.Query
{
    public class Query
    {
        public Filters Filters { get; }
        private IQueryStrategy QueryStrategy;
        public Query(Filters filters, IQueryStrategy queryStrategy)
        {
            Filters = filters;
            QueryStrategy = queryStrategy;
        }

        public List<Filterable> GetQueryResult(List<Filterable> filterables, List<Filterable> transactions)
        {
            if (QueryStrategy != null)
            {
                return QueryStrategy.getQueryResult(filterables, transactions);
            }

            return filterables;
        }
    }
}