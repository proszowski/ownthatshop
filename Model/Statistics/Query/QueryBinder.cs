using System;
using ownthatshop.Model.Statistics.Filter;
using ownthatshop.Model.Statistics.Query.Client;
using ownthatshop.Model.Statistics.Query.Product;

namespace ownthatshop.Model.Statistics.Query
{
    public class QueryBinder
    {
        public static Query bind(QueryDto queryDto)
        {
            var filters = FiltersUnpacker.UnpackDto(queryDto.FiltersDto);
            return new Query(filters, GetQuery(queryDto.QueryName));
        }

        private static IQueryStrategy GetQuery(QueryName? queryDtoQueryName)
        {
            if (queryDtoQueryName == null)
            {
                return null;
            }

            switch (queryDtoQueryName)
            {
                case QueryName.TheMostExpensiveProduct:
                    return new TheMostExpensiveProductsQueryStrategy();
                case QueryName.TheLessExpensiveProduct:
                    return new TheLessPopularProduct();
                case QueryName.TheMostPopularProduct:
                    return new TheMostPopularProduct();
                case QueryName.TheLessPopularProduct:
                    return new TheLessPopularProduct();
                case QueryName.BoughtTheBiggestAmountOfProducts:
                    return new BoughtTheBiggestAmountOfProductsQueryStrategy();
                default:
                    throw new ArgumentOutOfRangeException(nameof(queryDtoQueryName), queryDtoQueryName, null);
            }
        }
    }
}