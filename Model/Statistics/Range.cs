using System;

namespace ownthatshop.Model.Statistics
{
    public class Range<T> where T:IComparable
    {
        private readonly T _start;
        private readonly T _end;

        public Range(T start, T end)
        {
            _start = start;
            _end = end;
        }

        public Boolean Includes(T check)
        {
            return check.CompareTo(_start) >= 0 && check.CompareTo(_end) <= 0;
        }
    }
}