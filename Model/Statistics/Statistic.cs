using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ownthatshop.Model.Statistics.Filter;

namespace ownthatshop.Model.Statistics
{
    public class Statistic
    {
        public Statistic(List<Filterable> queryResult)
        {
            QueryResult = queryResult;
        }

        public List<Filterable> QueryResult { get; set; }
    }
}