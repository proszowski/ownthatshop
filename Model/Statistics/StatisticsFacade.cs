using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ownthatshop.Model.Client;
using ownthatshop.Model.Product;
using ownthatshop.Model.Statistics.Filter;
using ownthatshop.Model.Transaction;

namespace ownthatshop.Model.Statistics
{
    public class StatisticsFacade
    {
        private List<Client.Client> _clients;
        private List<Product.Product> _products;
        private List<Transaction.Transaction> _transactions;

        public StatisticsFacade(ClientFacade clientFacade, ProductFacade productFacade,
            TransactionFacade transactionFacade)
        {
            _clients = clientFacade.GetAllClients().Value;
            _products = productFacade.GetAllProducts().Value;
            _transactions = transactionFacade.GetAll().Value;
        }

        public ActionResult<Statistic> GetStatistics(Query.Query query, ResultType type)
        {
            var filters = query.Filters;
            var filteredClients = filters.Apply(new FilterableList<Client.Client>(_clients).Filterables);
            var filteredProducts = filters.Apply(new FilterableList<Product.Product>(_products).Filterables);
            var filteredTransactions =
                filters.Apply(new FilterableList<Transaction.Transaction>(_transactions).Filterables);

            var clients = filteredClients.Select(filterable => filterable.Entity).OfType<Client.Client>().ToList();
            var products = filteredProducts.Select(filterable => filterable.Entity).OfType<Product.Product>().ToList();
            var transactions = filteredTransactions.Select(filterable => filterable.Entity)
                .OfType<Transaction.Transaction>().ToList();

            filteredTransactions = TransactionsWithIncludedProductsAndClients(transactions, clients, products);
            filteredProducts = ProductsWithIncludedTransactions(products, transactions);
            filteredClients = ClientsWithIncludedTransactions(clients, transactions);

            switch (type)
            {
                case ResultType.Products:
                    return new Statistic(query.GetQueryResult(filteredProducts, filteredTransactions));
                case ResultType.Clients:
                    return new Statistic(query.GetQueryResult(filteredClients, filteredTransactions));
                case ResultType.Transactions:
                    return new Statistic(query.GetQueryResult(filteredTransactions, filteredTransactions));
                default:
                    return new Statistic(new List<Filterable>());
            }
        }

        private List<Filterable> ClientsWithIncludedTransactions(List<Client.Client> clients,
            List<Transaction.Transaction> transactions)
        {
            return clients
                .Where(c => transactions
                    .Select(t => t.ClientId)
                    .Contains(c.Id ?? 0))
                .Select(c => new Filterable(c))
                .ToList();
        }

        private List<Filterable> ProductsWithIncludedTransactions(IEnumerable<Product.Product> products,
            IEnumerable<Transaction.Transaction> transactions)
        {
            return products
                .Where(p => transactions
                    .SelectMany(t => t.TransactionDetails)
                    .Select(td => td.ProductId)
                    .Contains(p.Id ?? 0)
                )
                .Select(p => new Filterable(p))
                .ToList();
        }

        private static List<Filterable> TransactionsWithIncludedProductsAndClients(
            List<Transaction.Transaction> transactions, List<Client.Client> clients,
            List<Product.Product> products)
        {
            return transactions
                .Where(t => clients.Select(c => c.Id).Contains(t.ClientId))
                .Where(t => t.TransactionDetails
                    .Join(products, td => td.ProductId, p => p.Id, (td, p) => new
                    {
                        TransactionDetails = td,
                        Products = p
                    })
                    .Select(row => row.TransactionDetails.TransactionId).Contains(t.Id))
                .Select(t => new Filterable(t))
                .ToList();
        }
    }
}