namespace ownthatshop.Model.Statistics
{
    public enum ResultType
    {
        Products,
        Clients,
        Transactions
    }
}